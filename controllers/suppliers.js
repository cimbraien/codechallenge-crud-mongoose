const { supplier } = require("../models");

class Supplier {
  async getAllSupplier(req, res, next) {
    try {
      const data = await supplier.find();
      if (data.length === 0) {
        return next({ message: "Suppliers is not found", statusCode: 404 });
      }

      res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  }

  async getOneSupplier(req, res, next) {
    try {
      const data = await supplier.findOne({
        _id: req.params.id,
      });
      if (!data) {
        return next({ message: "Supplier is not found", statusCode: 404 });
      }

      res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  }

  async createSupplier(req, res, next) {
    try {
      const newData = await supplier.create(req.body);

      const data = await supplier.findOne({
        _id: newData._id,
      });

      res.status(201).json({ data });
    } catch (error) {
      next(error);
    }
  }

  async updateSupplier(req, res, next) {
    try {
      const newData = await supplier.findOneAndUpdate(
        { _id: req.params.id },
        req.body,
        { new: true }
      );

      if (!newData) {
        return next({ statusCode: 404, message: "Supplier is not found" });
      }

      res.status(201).json({ newData });
    } catch (error) {
      next(error);
    }
  }

  async deleteSupplier(req, res, next) {
    try {
      //   for soft delete
      const data = await supplier.deleteById(req.params.id);

      if (data.nModified === 0) {
        return next({ statusCode: 404, message: "Supplier is not found" });
      }

      res.status(200).json({ message: "Supplier successfully deleted" });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = new Supplier();
